from sdf import *  # pip install git+https://gitlab.com/nobodyinperson/sdf

#  _    _  _______          __   _____                        _
# | |  | |/ ____\ \        / /  / ____|                      (_)
# | |__| | (___  \ \  /\  / /  | (___   ___ _ __ _____      _____   _____ _ __ ___  ___
# |  __  |\___ \  \ \/  \/ /    \___ \ / __| '__/ _ \ \ /\ / / \ \ / / _ \ '__/ __|/ _ \
# | |  | |____) |  \  /\  /     ____) | (__| | |  __/\ V  V /| |\ V /  __/ |  \__ \  __/
# |_|  |_|_____/    \/  \/     |_____/ \___|_|  \___| \_/\_/ |_| \_/ \___|_|  |___/\___|
#

# Generate STLs by calling `HSW...().save("path.stl")`

HSW_BASE_HEX_HOLE_INNER_DIAMETER = 20
HSW_BASE_HEX_HOLE_OUTER_DIAMETER = HSW_BASE_HEX_HOLE_INNER_DIAMETER / 0.866
HSW_BASE_HEX_HOLE_SIDE_LENGTH = HSW_BASE_HEX_HOLE_OUTER_DIAMETER / 2
HSW_BASE_BACK_HEX_HOLE_INNER_DIAMETER = 21.5  # 📏
HSW_BASE_BACK_HEX_HOLE_OUTER_DIAMETER = HSW_BASE_BACK_HEX_HOLE_INNER_DIAMETER / 0.866
HSW_BASE_BACK_HEIGHT = 2.5  # 📏
HSW_BASE_SPAR_WIDTH = 3.6
HSW_HOLE_DISTANCE_VERTICAL = HSW_BASE_HEX_HOLE_INNER_DIAMETER + HSW_BASE_SPAR_WIDTH
HSW_HOLE_DISTANCE = HSW_HOLE_DISTANCE_VERTICAL  # DEPRECATED!
HSW_HOLE_DISTANCE_HORIZONTAL = 40.88
HSW_INSERT_TOP_THICKNESS = 2.5
HSW_BASE_HEIGHT = 8

HSW_COORDS_UP = HSW_HOLE_DISTANCE_VERTICAL * Y
HSW_COORDS_UPRIGHT = (
    np.cos(units("30°")).m * HSW_HOLE_DISTANCE_VERTICAL * X
    + np.sin(units("30°")).m * HSW_HOLE_DISTANCE_VERTICAL * Y
)

HSW_HOLLOW_INSERT_HOLE_OUTER_DIAMETER = 15.47
HSW_HOLLOW_INSERT_HOLE_INNER_DIAMETER = 13.4

HSW_SCREWIVERSE_THREAD_DIAMETER = 15
HSW_SCREWIVERSE_THREAD_PITCH = 4
HSW_SCREWIVERSE_THREAD_OFFSET = 0.5
HSW_SCREWIVERSE_THREAD_K = 2
HSW_SCREWIVERSE_ROUNDNESS = 1
HSW_SCREWIVERSE_SCREW_HEAD_HEIGHT = 5
HSW_SCREWIVERSE_SCREW_HEAD_RADIUS = (
    HSW_BASE_BACK_HEX_HOLE_OUTER_DIAMETER / 2 + HSW_BASE_SPAR_WIDTH / 2 - 0.1
)
HSW_SCREWIVERSE_THREAD_CLEARANCE = 0.4
HSW_SCREWIVERSE_NUT_DIAMETER = (
    HSW_SCREWIVERSE_THREAD_DIAMETER + 2 * HSW_SCREWIVERSE_THREAD_OFFSET + 6
)
HSW_SCREWIVERSE_CLEARANCE = 0.3
HSW_SCREWIVERSE_THICKNESS = 3


def HSWScrewiverseThread(length=None, upmarker=False, **kwargs):
    if length:
        thread = HSWScrewiverseThread(**{**kwargs, **dict(upmarker=upmarker)})
        thread &= slab(z0=0)
        thread &= slab(z1=length).k(HSW_SCREWIVERSE_THREAD_K)
        return thread
    thread = Thread(
        **{
            **dict(
                pitch=HSW_SCREWIVERSE_THREAD_PITCH,
                diameter=HSW_SCREWIVERSE_THREAD_DIAMETER,
                offset=HSW_SCREWIVERSE_THREAD_OFFSET,
            ),
            **kwargs,
        }
    )
    if upmarker:
        thread -= slab(dx=1, y0=HSW_SCREWIVERSE_THREAD_DIAMETER / 4).k(
            HSW_SCREWIVERSE_THREAD_K
        )
    return thread


def HSWScrewiverseScrew(
    length=HSW_BASE_HEIGHT + HSW_INSERT_TOP_THICKNESS, thread=None, vase_mode=False
):
    thread = thread or HSWScrewiverseThread()
    head = (
        hexagon(
            HSW_HOLE_DISTANCE / 2
            - HSW_SCREWIVERSE_CLEARANCE
            - HSW_SCREWIVERSE_ROUNDNESS
        )
        .dilate(HSW_SCREWIVERSE_ROUNDNESS)
        .extrude()
        .modulate_between(
            HSW_SCREWIVERSE_ROUNDNESS * Z,
            ORIGIN,
            -HSW_SCREWIVERSE_ROUNDNESS * ease.linear,
        )
    )
    if vase_mode:
        head = head.modulate_between(
            (z := (HSW_SCREWIVERSE_SCREW_HEAD_HEIGHT - HSW_SCREWIVERSE_ROUNDNESS) * Z),
            z + HSW_BASE_HEX_HOLE_INNER_DIAMETER / 2 * Z,
            -HSW_BASE_HEX_HOLE_INNER_DIAMETER / 2 * ease.smoothstep,
        )
    else:
        head = head.modulate_between(
            (HSW_SCREWIVERSE_SCREW_HEAD_HEIGHT - HSW_SCREWIVERSE_ROUNDNESS) * Z,
            HSW_SCREWIVERSE_SCREW_HEAD_HEIGHT * Z,
            -HSW_SCREWIVERSE_ROUNDNESS * ease.linear,
        )
    head &= slab(
        z0=0,
        z1=(
            HSW_SCREWIVERSE_SCREW_HEAD_HEIGHT + length
            if vase_mode
            else HSW_SCREWIVERSE_SCREW_HEAD_HEIGHT
        ),
    )
    head -= (
        slab(
            z1=HSW_SCREWIVERSE_SCREW_HEAD_HEIGHT - 1,
            dx=1.5,
            dy=HSW_BASE_HEX_HOLE_INNER_DIAMETER / 2,
        )
        .modulate_between(
            HSW_SCREWIVERSE_ROUNDNESS / 2 * Z,
            ORIGIN,
            HSW_SCREWIVERSE_ROUNDNESS / 2 * ease.linear,
        )
        .circular_array(4)
    )
    thread &= slab(z1=length).k(HSW_SCREWIVERSE_THREAD_K)
    thread &= slab(z0=0)
    if vase_mode:
        screw = head | thread.translate(
            (
                HSW_SCREWIVERSE_SCREW_HEAD_HEIGHT
                + (HSW_BASE_HEX_HOLE_INNER_DIAMETER - HSW_SCREWIVERSE_THREAD_DIAMETER)
                * np.sin(units("45°")).m
            )
            * Z
        ).k(HSW_SCREWIVERSE_THREAD_K)
    else:
        screw = head | thread.translate(HSW_SCREWIVERSE_SCREW_HEAD_HEIGHT * Z)
    return screw


def HSWScrewiverseNut(
    diameter=HSW_SCREWIVERSE_NUT_DIAMETER,
    thickness=HSW_SCREWIVERSE_SCREW_HEAD_HEIGHT,
    roundness=1,
):
    nut = hexagon(diameter / 2 - roundness).dilate(roundness).extrude(np.inf)
    for i in [-1, 1]:
        nut = nut.modulate_between(
            i * (thickness / 2 - roundness) * Z,
            i * thickness / 2 * Z,
            e=-roundness * ease.linear,
        )
    nut &= slab(z0=-thickness / 2, z1=thickness / 2)
    nut -= HSWScrewiverseThread().dilate(HSW_SCREWIVERSE_THREAD_CLEARANCE)
    return nut


def HSWScrewiverseStopNut(
    diameter=HSW_SCREWIVERSE_NUT_DIAMETER,
    length=20,
    thickness=HSW_SCREWIVERSE_SCREW_HEAD_HEIGHT,
):
    nut = capsule(ORIGIN, length * X, diameter=diameter)
    nut -= HSWScrewiverseThread().dilate(HSW_SCREWIVERSE_THREAD_CLEARANCE)
    # chamfers
    nut = nut.modulate_between(
        -(thickness / 2 - (r := thickness / 5)) * Z,
        -thickness / 2 * Z,
        -r * ease.linear,
    )
    nut = nut.modulate_between(
        (thickness / 2 - (r := thickness / 5)) * Z, thickness / 2 * Z, -r * ease.linear
    )
    nut &= slab(dz=thickness)
    return nut


def HSWScrewiverseThreadedInsert(angles=None):
    """
    If called with `angles=[degree1,degree2,...]`, it makes a centered insert
    with others rotated around it. Only multiples of 60° make sense.
    """
    if angles:
        insert = HSWScrewiverseThreadedInsert()
        inserts = [insert]  # start with centered insert
        for angle in angles or []:
            inserts.append(
                insert.rotate(-angle * units.degree, Z)
                .translate(HSW_HOLE_DISTANCE * Y)
                .rotate(angle * units.degree, Z)
            )
        # merge inserts
        insert = union(*inserts, radius=4 * HSW_SCREWIVERSE_CLEARANCE)
        # cut away weirdness from k()-rounding
        insert &= slab(z0=-HSW_INSERT_TOP_THICKNESS)
        return insert

    CUT_THICKNESS = 1
    STRETCH = (
        HSW_BASE_BACK_HEX_HOLE_INNER_DIAMETER - HSW_BASE_HEX_HOLE_INNER_DIAMETER
    ) / 2
    # rim
    toprim = hexagon(
        r := HSW_HOLE_DISTANCE / 0.866 / 2 - HSW_SCREWIVERSE_CLEARANCE
    ).extrude() & slab(z1=HSW_INSERT_TOP_THICKNESS)
    # add a notch so it's clear what's 'up'
    toprim -= RegularPolygonColumn(
        n=3,
        r=(
            HSW_HOLE_DISTANCE / 2
            - HSW_SCREWIVERSE_CLEARANCE
            - HSW_SCREWIVERSE_THREAD_DIAMETER / 2
            - HSW_SCREWIVERSE_THREAD_CLEARANCE
        )
        / 2
        - 2 * HSW_SCREWIVERSE_CLEARANCE,
    ).translate(
        np.mean(
            [
                HSW_HOLE_DISTANCE / 2 - HSW_SCREWIVERSE_CLEARANCE,
                HSW_SCREWIVERSE_THREAD_DIAMETER / 2 + HSW_SCREWIVERSE_THREAD_CLEARANCE,
            ]
        )
        * Y
    ) & slab(
        z1=HSW_INSERT_TOP_THICKNESS / 2
    )
    body = hexagon(HSW_BASE_HEX_HOLE_OUTER_DIAMETER / 2).extrude()
    body = body.modulate_between(
        (HSW_BASE_HEIGHT - HSW_BASE_BACK_HEIGHT - 1) * Z,
        (HSW_BASE_HEIGHT) * Z,
        e=((STRETCH + 0.1) * ease.smoothstep.chain()).append(
            (-ease.smoothstep.transition(ease.linear) + (STRETCH + 0.1))
        ),
    ).modulate_between(
        ORIGIN,
        HSW_BASE_HEIGHT * Z,
        e=-STRETCH * ease.linear,
    )
    body &= slab(z0=0, z1=HSW_INSERT_TOP_THICKNESS + HSW_BASE_HEIGHT)
    body -= (slab(dx=CUT_THICKNESS).circular_array(3).rotate(units("30°"), Z)).k(
        CUT_THICKNESS / 2
    )
    insert = body
    insert |= toprim.translate(-HSW_INSERT_TOP_THICKNESS * Z).k(CUT_THICKNESS)
    insert -= (
        HSWScrewiverseThread()
        # have the thread 'start' at the top, not inside the base
        .translate(-HSW_INSERT_TOP_THICKNESS * Z)
        .modulate_between(ORIGIN, HSW_BASE_HEIGHT * Z, e=-STRETCH * ease.linear)
        .dilate(HSW_SCREWIVERSE_THREAD_CLEARANCE)
    )
    insert -= (
        slab(dx=CUT_THICKNESS, dy=HSW_BASE_HEX_HOLE_OUTER_DIAMETER - CUT_THICKNESS)
        .circular_array(3)
        .rotate(units("30°"), Z)
    ).k(CUT_THICKNESS / 2)
    # cut insert from bottom and top
    insert &= slab(z0=-HSW_INSERT_TOP_THICKNESS, z1=HSW_BASE_HEIGHT)
    return insert


def HSWScrewiverseScrewAttachmentRodWithDips(
    diameter=HSW_SCREWIVERSE_THREAD_DIAMETER, length=20, dips=[], roundness=1
):
    screw = HSWScrewiverseThread() & slab(z0=0)
    screw &= slab(z1=HSW_BASE_HEIGHT + HSW_INSERT_TOP_THICKNESS).k(roundness)
    screw |= hexagon(radius=(screw_head_radius := diameter / 2) - roundness).extrude(
        np.inf
    ).dilate(roundness) & slab(z1=0)
    screw = screw.modulate_between(
        (-length + roundness) * Z, -length * Z, e=ease.linear * -roundness
    )
    screw &= slab(z0=-length)
    dipdepth = HSW_HOLLOW_INSERT_HOLE_OUTER_DIAMETER / 2 * 0.5
    diplength = 3 * dipdepth
    for dip in dips:
        screw = screw.modulate_between(
            -(dip - diplength / 2) * Z,
            -(dip + diplength / 2) * Z,
            e=ease.in_out_sine.symmetric * (-screw_head_radius * 0.3),
        )
    return screw


def HSWScrewiverseTesaPowerstripsMount(
    for_hole=HSW_BASE_HEX_HOLE_OUTER_DIAMETER, pinlength=HSW_BASE_HEIGHT, shape=hexagon
):
    pinradius = for_hole / 2
    plate = rounded_box((box_size := np.array([24, 45, 4])), 1) & plane(UP)
    pin = (
        shape(pinradius - (r := pinradius / 6))
        .dilate(r)
        .erode(HSW_SCREWIVERSE_CLEARANCE)
        .extrude()
    ).modulate_between(
        (pinlength - HSW_SCREWIVERSE_ROUNDNESS) * Z,
        pinlength * Z,
        -HSW_SCREWIVERSE_ROUNDNESS * ease.linear,
    )
    pin &= slab(z0=0, z1=pinlength)
    if for_hole < HSW_BASE_HEX_HOLE_OUTER_DIAMETER:
        thread = HSWScrewiverseThread(diameter=10)
    else:
        thread = HSWScrewiverseThread()
    pin -= thread.dilate(HSW_SCREWIVERSE_THREAD_CLEARANCE)
    holdscrew = HSWScrewiverseScrew(
        thread=thread,
        length=pinlength,
    )
    bracket = plate | pin.translate(box_size / 2 * Z)
    return bracket | holdscrew.translate(box_size[0] * 1.5 * X)


def HSWScrewiverseTentacleHook(
    end=40 * X + 20 * Y,
    p1=None,
    p2=None,
    mirrored=False,
    symmetric_pair=False,
    radius=None,
):
    """
    Make a bezier hook

    Args:
        end, p1, p2: control points. start point is ORIGIN.
        mirrored (bool): whether to mirror hook on Y axis (not the thread!)
        symmetric_pair (bool): make it two symmetric hooks
    """
    if p1 is None:
        p1 = ORIGIN
    if p2 is None:
        p2 = end
    tip_thickness = HSW_SCREWIVERSE_THREAD_DIAMETER / 2
    z = tip_thickness / 2 * Z
    hook = bezier(
        ORIGIN,
        p1 - 1 / 3 * z,
        p2 - 2 / 3 * z,
        end - z,
        diameter=ease.linear.between(HSW_BASE_HEX_HOLE_INNER_DIAMETER, tip_thickness),
        # radius=thickness / 4,
    ) & slab(x0=0).k(k)
    if mirrored:
        hook = hook.mirror(Y)
    # add thread for attachment
    thread = HSWScrewiverseThread(upmarker=False)
    if mirrored:
        thread = thread.rotate(units("180°"))
    thread &= plane(UP)
    thread &= slab(z1=HSW_BASE_HEIGHT + HSW_INSERT_TOP_THICKNESS).k(
        HSW_SCREWIVERSE_THREAD_K
    )
    hook |= thread.orient(-X)
    hook &= slab(
        z0=-(HSW_SCREWIVERSE_THREAD_DIAMETER / 2 - 1.5)
    )  # cutaway for printability
    if symmetric_pair:
        return hook.translate(
            (HSW_BASE_HEIGHT + HSW_INSERT_TOP_THICKNESS) * 1.5 * X
        ) | HSWScrewiverseTentacleHook(
            end=end, p1=p1, p2=p2, mirrored=not mirrored, symmetric_pair=False
        ).translate(
            (HSW_BASE_HEIGHT + HSW_INSERT_TOP_THICKNESS) * 1.5 * X
        ).rotate(
            units("180°")
        )
    return hook


def HSWScrewiverseRing(inner_diameter=20, thickness=HSW_SCREWIVERSE_THICKNESS):
    thread = HSWScrewiverseThread() & slab(
        z0=-inner_diameter / 2, z1=HSW_BASE_HEIGHT + HSW_INSERT_TOP_THICKNESS
    ).k(HSW_SCREWIVERSE_THREAD_K)
    ring = (hole := cylinder(diameter=inner_diameter)).dilate(thickness)
    ring &= slab(dz=HSW_SCREWIVERSE_THREAD_DIAMETER).k(HSW_SCREWIVERSE_ROUNDNESS)
    ring |= (
        thread.rotate(units("180°"), Z)
        .orient(Y)
        .translate((inner_diameter / 2 + thickness) * Y)
        .k(HSW_SCREWIVERSE_ROUNDNESS)
    )
    ring -= hole.k(thickness)
    ring &= slab(
        z0=-HSW_SCREWIVERSE_THREAD_DIAMETER / 2 + 2,
        z1=HSW_SCREWIVERSE_THREAD_DIAMETER / 2,
    )
    return ring
