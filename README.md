# 🔩💫  Honeycomb Storage Wall - Screwiverse

This is my add-on standard for [RostaP's excellent Honeycomb Storage Wall standard](https://www.printables.com/model/152592-honeycomb-storage-wall#preview) that adds threaded inserts and attachments that are easily moved around - in contrast to the default press-fit inserts.

The models are available [on Printables.com](https://www.printables.com/model/470787-hsw-screwiverse) but you can also build them yourself:

```bash
# get the repository
git clone --recursive https://gitlab.com/nobodyinperson/hsw-screwiverse
cd hsw-screwiverse
# install dependencies
pip install jupyterlab ./sdf
# start Jupyter
jupyter lab
# open the hsw-screwiverse.ipynb  notebook and run it
# models are created in the stl/ directory
```

The source code for the models are in the `hsw_screwiverse.py` Python module.
